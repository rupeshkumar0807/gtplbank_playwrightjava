package Playwright.gtplBankDemo.failedTestcases;

import java.util.ArrayList;

import org.testng.TestNG;

public class RunFailedTestcases 
{
	
	public void runFailedTestcases()
	{
		//create object of testng class
		TestNG runner = new TestNG();
		
		//declare arraylist to store the testng-failed.xml file path
		ArrayList<String> list = new ArrayList<String>();
		
		list.add("C:\\Users\\Rupesh\\eclipse-workspace\\gtplBankDemo\\test-output\\testng-failed.xml");
		
		runner.setTestSuites(list);
		
		runner.run();
	}
	
}
