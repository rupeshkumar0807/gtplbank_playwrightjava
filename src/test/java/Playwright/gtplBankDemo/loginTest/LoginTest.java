package Playwright.gtplBankDemo.loginTest;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import Playwright.gtplBankDemo.base.BaseClass;
import Playwright.gtplBankDemo.excelData.ExcelDataHashmap;
import Playwright.gtplBankDemo.loginPage.LoginPage;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class LoginTest extends BaseClass
{
	//declaring global variables
	LoginPage gLoginPage;
	static String gUsername;
	static String gPassword;

	public static void retriveData() 
	{
		Map<String, String> testData = ExcelDataHashmap.getExcelData();

		for (Entry<String, String> map : testData.entrySet())
		{
			if (map.getKey().equals("Username"))
			{
				gUsername = map.getValue();
			}
			if (map.getKey().equals("Password"))
			{
				gPassword = map.getValue();
			}
		}

	}

	@Test(priority = 0, groups = { "positive", "sanity" }, alwaysRun = true)
	@Description("Verify and Validate the functionality of login Page with Valid Credentials")
	@Severity(SeverityLevel.BLOCKER)
	public void loginWithValidCredentials() throws InterruptedException 
	{
		// retriveData() function is used to carry forward gUsername and gPassword to dashboard test class
		retriveData();
		System.out.println(gUsername + " " + gPassword);
		// passing current session page instance to the Login Page POM
		gLoginPage = new LoginPage(page);
		// calling doLoginmethod and passing valid credentials
		gLoginPage.doLogin(gUsername, gPassword);

	}

}
