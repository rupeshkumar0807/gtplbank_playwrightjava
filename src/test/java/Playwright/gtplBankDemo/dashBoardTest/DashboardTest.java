package Playwright.gtplBankDemo.dashBoardTest;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Playwright.gtplBankDemo.base.BaseClass;
import Playwright.gtplBankDemo.dashBoard.DashboardPage;
import Playwright.gtplBankDemo.loginTest.LoginTest;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class DashboardTest extends BaseClass
{
	//creating refernce variable for LoginTest class
	LoginTest gLoginTest;
	DashboardPage gDashboardPage;
	
	@BeforeMethod()
	public void navigateToDashboard() throws InterruptedException
	{
		gLoginTest = new LoginTest();
		gLoginTest.loginWithValidCredentials();
	}
	
	@Test(priority = 1,groups = {"positive","sanity"})
	@Description("Verify whether user is able to navigate to the NewCustomer Page or not")
	@Severity(SeverityLevel.BLOCKER)
	public void verifyNavigationToTheNewCustomerPage()
	{
		gDashboardPage = new DashboardPage(page); 

		gDashboardPage.verifyNavigationNewCustomer();
	}
	
	@Test(priority = 2,groups = {"positive","sanity"})
	@Description("Verify whether user is able to navigate to the EditCustomer Page or not")
	@Severity(SeverityLevel.BLOCKER)
	public void verifyNavigationToTheEditCustomerPage()
	{
		gDashboardPage = new DashboardPage(page); 

		gDashboardPage.verifyNavigationEditCustomer();
	}
	
	@Test(priority = 3,groups = {"positive","sanity"})
	@Description("Verify whether user is able to navigate to the Delete Customer Page or not")
	@Severity(SeverityLevel.BLOCKER)
	public void verifyNavigationToTheDeleteCustomer()
	{
		gDashboardPage = new DashboardPage(page); 

		gDashboardPage.verifyNavigationDeleteCustomer();
	}
	
	@Test(priority = 4,groups = {"positive","sanity"})
	@Description("Verify whether user is able to navigate to the NewAccount Page or not")
	@Severity(SeverityLevel.BLOCKER)
	public void verifyNavigationToTheNewAccount()
	{
		gDashboardPage = new DashboardPage(page); 

		gDashboardPage.verifyNavigationNewAccount();
	}
	
	@Test(priority = 5,groups = {"positive","sanity"})
	@Description("Verify whether user is able to navigate to the EditAccount Page or not")
	@Severity(SeverityLevel.BLOCKER)
	public void verifyNavigationToTheEditAccount()
	{
		gDashboardPage = new DashboardPage(page); 

		gDashboardPage.verifyNavigationEditAccount();
	}
	
	@Test(priority = 6,groups = {"positive","sanity"})
	@Description("Verify whether user is able to navigate to the DeleteAccount Page or not")
	@Severity(SeverityLevel.BLOCKER)
	public void verifyNavigationToTheDeleteAccount()
	{
		gDashboardPage = new DashboardPage(page); 

		gDashboardPage.verifyNavigationgDeleteAccount();
	}
	
	@Test(priority = 7,groups = {"positive","sanity"})
	@Description("Verify whether user is able to navigate to the MiniStatement Page or not")
	@Severity(SeverityLevel.BLOCKER)
	public void verifyNavigationToTheMiniStatement()
	{
		gDashboardPage = new DashboardPage(page); 

		gDashboardPage.verifyNavigationMiniStatement();
	}
	
	@Test(priority = 8,groups = {"positive","sanity"})
	@Description("Verify whether user is able to navigate to the CustomisedStatement Page or not")
	@Severity(SeverityLevel.BLOCKER)
	public void verifyNavigationToTheCustomisedStatement()
	{
		gDashboardPage = new DashboardPage(page); 

		gDashboardPage.verifyNavigationCustomisedStatement();
	}
	
	@Test(priority = 9,groups = {"positive","sanity"})
	@Description("Verify whether user is able to logout or not")
	@Severity(SeverityLevel.BLOCKER)
	public void verifyLogout()
	{
		gDashboardPage = new DashboardPage(page); 

		gDashboardPage.verifyLogout();
	}
}
