package Playwright.gtplBankDemo.propertyData;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Properties;

public class PropertyReader 
{
	private String propertyName = null;
	private Properties props;
	private String configproppath = System.getProperty("user.dir") + "//Property//config.properties";
	

	// private method created to load property file
	private void loadProperty() 
	{
		try
		{
			props = new Properties();
			FileInputStream fis = new FileInputStream(configproppath);
			props.load(fis);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	//Public method created to access outside class.This method load property file and store Property file data in HashMap 	
	
	public HashMap<String, String> getPropertyAsHashMap()
	{
		loadProperty();
		HashMap<String, String> map = new HashMap<String, String>();

		for (Entry<Object, Object> entry : props.entrySet())
		{
			map.put(entry.getKey().toString(),entry.getValue().toString());
		}
		
		return map;
	}
	
}
