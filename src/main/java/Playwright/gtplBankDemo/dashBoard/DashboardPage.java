package Playwright.gtplBankDemo.dashBoard;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.microsoft.playwright.Page;

import Playwright.gtplBankDemo.base.BaseClass;
import Playwright.gtplBankDemo.excelData.ExcelDataHashmap;
import Playwright.gtplBankDemo.propertyData.PropertyReader;

public class DashboardPage extends BaseClass
{
	//creating reference variable og page
	protected Page page;
	
	static final String Dashboard_SHEET = "DashboardSheet";
	
	//stage 1  : declaring the webElement
	PropertyReader gPropertyReader = new PropertyReader();
	HashMap<String,String> gMap = gPropertyReader.getPropertyAsHashMap();
	
	String gNewCustomer =  gMap.get("NewCustomer");
	String gEditCustomer = gMap.get("NewCustomer");
	String gDeleteCustomer = gMap.get("NewCustomer");
	String gNewAccount = gMap.get("NewCustomer");
	String gEditAccount = gMap.get("NewCustomer");
	String gDeleteAccount = gMap.get("NewCustomer");
	String gMiniStatement = gMap.get("NewCustomer");
	String gCustomisedStatement = gMap.get("NewCustomer");
	String gLogout = gMap.get("NewCustomer");

	
	//Stage 2 - Intialization of WebElement by creating constructor.
	public DashboardPage(Page page)
	{
		//this.page = page - passing session page instance to the dashboard page 
		this.page = page;
	}
	
	//Stage 3 - Utilization stage by creating methods to perform actions on webelement
	
	public void verifyNavigationNewCustomer()
	{
		page.locator(gNewCustomer).click();
	}
	
	public void verifyNavigationEditCustomer()
	{
		page.locator(gEditCustomer).click();
	}

	public void verifyNavigationDeleteCustomer()
	{
		page.locator(gDeleteCustomer).click();	
	}
	
	public void verifyNavigationNewAccount()
	{
		page.locator(gNewAccount).click();	
	}
	
	public void verifyNavigationEditAccount()
	{
		page.locator(gEditAccount).click();	
	}
	
	public void verifyNavigationgDeleteAccount()
	{
		page.locator(gDeleteAccount).click();	
	}
	
	public void verifyNavigationMiniStatement()
	{
		page.locator(gMiniStatement).click();	
	}
	
	public void verifyNavigationCustomisedStatement()
	{
		page.locator(gCustomisedStatement).click();	
	}
	
	public void verifyLogout()
	{
		page.locator(gLogout).click();	
	}
}
