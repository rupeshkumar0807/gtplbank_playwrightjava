package Playwright.gtplBankDemo.loginPage;

import java.util.HashMap;

import com.microsoft.playwright.Page;

import Playwright.gtplBankDemo.base.BaseClass;
import Playwright.gtplBankDemo.propertyData.PropertyReader;

//extends keyword is used to inherits the methods from BaseClass
public class LoginPage extends BaseClass
{
	// creating reference variable of page
	protected Page page;
	PropertyReader gPropertyReader = new PropertyReader();
	HashMap<String,String> gMap = gPropertyReader.getPropertyAsHashMap();
	
	static final String LOGIN_SHEET = "Login";

	// stage 1 : declaring the webElement
	String gUsernameLocator = gMap.get("usernameLocator");
	String gPasswordLocator = gMap.get("passwordLocator");
	String gLoginBtnLocator = gMap.get("loginBTNLocator");


	// Stage 2 - Intialization of WebElement by creating constructor.
	public LoginPage(Page page)
	{
		// this keyword is used to call the present class constructor
		// this.page = page - passing session page instance to the login page
		this.page = page;
	}

	// Stage 3 - Utilization stage by creating methods to perform actions on webelement

	public void doLogin(String pUsername, String pPassword) throws InterruptedException {
		
		page.locator(gUsernameLocator).fill(pUsername);
		page.locator(gPasswordLocator).fill(pPassword);
		page.waitForSelector(gLoginBtnLocator, new Page.WaitForSelectorOptions().setTimeout(3000.0));
		page.locator(gLoginBtnLocator).click();
		Thread.sleep(2000);
	}

}
