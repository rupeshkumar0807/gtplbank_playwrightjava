package Playwright.gtplBankDemo.excelData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelDataHashmap
{
	protected static String excelPath = System.getProperty("user.dir") + "./InputData//GTPLBank_TestData.xlsx";
	private static FileInputStream fis;
	private static XSSFWorkbook workbook;
	private static XSSFSheet sheet;
	private static XSSFRow row;

	public static Map<String, String> getExcelData()
	{
		// Creates a new File instance by converting the given pathname string
		File excelFile = new File(excelPath);
		try
		{
			// Creates a new File instance by converting the given pathname string
			fis = new FileInputStream(excelFile);

		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		try
		{
			// XSSFWorkbook. It is a class that is used to represent both high and low level Excel file formats.
			workbook = new XSSFWorkbook(fis);
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}

		// This is a class which represents high level representation of excel spreadsheet
		sheet = workbook.getSheet("Login");
		
		// to find number of rows present in the sheet
		int lastRowNumber = sheet.getLastRowNum();
		
		// to store the test data(rowvalue,columnvalue)
		HashMap<String, String> testDataMap = new HashMap<String, String>();

		for (int i = 1; i < lastRowNumber; i++)
		{
			// t
			Row rows = sheet.getRow(i);
			Cell keyCell = rows.getCell(0);
			String key = keyCell.getStringCellValue().trim();

			Cell valueCell = rows.getCell(2);
			String value = valueCell.getStringCellValue().trim();

			testDataMap.put(key, value);
		}
		
		return testDataMap;
	}

}
