package Playwright.gtplBankDemo.base;

import java.nio.file.Paths;

import com.microsoft.playwright.Page;

public class Utility 
{
	protected Page page;
	
	public Utility(Page page)
	{
		this.page = page;
	}
	
	protected static final String USER_DIRECTORY = "D:\\GTPLBank\\";
	
	public static void takeScreenshotForFailedTestcases(Page page,String screenshotName)
	{
		page.screenshot(new Page.ScreenshotOptions().setPath(Paths.get(USER_DIRECTORY+"\\FailedScreenshots\\"+screenshotName+".png")).setFullPage(true));
		//page.screenshot(new Page.ScreenshotOptions().setPath(Paths.get(USER_DIRECTORY+screenshotName+".png")).setFullPage(true));
	}
}
